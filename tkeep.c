#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define __USE_XOPEN
#include <time.h>

#define CHAR_LEN 256
#define VIEW_LEN 63356
#define DAY_LEN 10
#define TIME_LEN 5

char tkeep_path_config[CHAR_LEN];
char tkeep_path_history[CHAR_LEN];

static int control_arg(int argc, char* argv[]);
static int memcpy_day(char* day, char* argv);
static void add_history(char* arg_time, char* arg_job);
static void modify_history(char* arg_line, char* arg_day, char* arg_time, char* arg_job);
static int delete_history(int d_line, char* str_day);
static void read_history(char* day);
static char* read_line(char* line);
static void parse_job(char* id, char* str);
static void init_paths();

int main(int argc, char* argv[])
{
    init_paths();
    control_arg(argc, argv);
    return 0;
}

static int control_arg(int argc, char* argv[])
{
    char str_day[DAY_LEN];
    memset(str_day, 0, DAY_LEN);

    // default
    if (argc == 1) {
        read_history(NULL);
        return 0;
    }

    // add
    if (argc > 1 && !memcmp(argv[1], "a", strlen(argv[1]))) {
        if (argc == 2) {
            add_history("0", NULL);
            read_history(NULL);
            return 0;
        }
        if (argc == 3) {
            add_history(argv[2], NULL);
            read_history(NULL);
            return 0;
        }
        if (argc == 5 && !memcmp(argv[3], "t", strlen(argv[3]))) {
            add_history(argv[2], argv[4]);
            read_history(NULL);
            return 0;
        }
    }

    // modify
    if (argc > 2 && !memcmp(argv[1], "m", strlen(argv[1]))) {
        if (argc == 5 && !memcmp(argv[3], "d", strlen(argv[3]))) {
            modify_history(argv[2], argv[4], NULL, NULL);
            read_history(NULL);
            return 0;
        }
        if (argc == 5 && !memcmp(argv[3], "t", strlen(argv[3]))) {
            modify_history(argv[2], NULL, argv[4], NULL);
            read_history(NULL);
            return 0;
        }
        if (argc == 5 && !memcmp(argv[3], "j", strlen(argv[3]))) {
            modify_history(argv[2], NULL, NULL, argv[4]);
            read_history(NULL);
            return 0;
        }
    }

    // delete
    if (argc == 3 && !memcmp(argv[1], "d", strlen(argv[1]))) {
        if (delete_history(atoi(argv[2]), str_day)) {
            read_history(str_day);
        } else {
            read_history(NULL);
        }
        return 0;
    }

    // read
    if (argc == 3 && !memcmp(argv[1], "r", strlen(argv[1]))) {
        if (memcpy_day(str_day, argv[2]) != 0) {
            read_history(str_day);
        } else {
            read_history(NULL);
        }
        return 0;
    }

    // err
    printf("check operation\n");
    printf("commands: a(add) r(read) d(delete)\n");
    printf("need these commands: a(add) r(read) d(delete)\n");
    return 0;
}

static int memcpy_day(char* day, char* argv)
{
    struct tm buf;
    memset(&buf, 0, sizeof(struct tm));
    if (strptime(argv, "%y/%m/%d", &buf) != NULL) {
        sprintf(day, "%04d/%02d/%02d", buf.tm_year + 1900, buf.tm_mon + 1, buf.tm_mday);
        return 1;
    }
    return 0;
}

static void modify_history(char* arg_line, char* arg_day, char* arg_time, char* arg_job)
{
    FILE* fp_history = NULL;
    struct tm buf;
    char* view = NULL;
    char line[CHAR_LEN];
    char str_day[DAY_LEN + 1];
    char str_time[TIME_LEN + 1];
    int n_line = 0;

    view = (char*)malloc(VIEW_LEN);
    memset(view, 0, VIEW_LEN);
    memset(line, 0, CHAR_LEN);
    memset(str_day, 0, DAY_LEN + 1);
    memset(str_time, 0, TIME_LEN + 1);

    if ((arg_day != NULL) && (strptime(arg_day, "%y/%m/%d", &buf) != NULL)) {
        sprintf(str_day, "%04d/%02d/%02d", buf.tm_year + 1900, buf.tm_mon + 1, buf.tm_mday);
    }

    if ((arg_time != NULL) && (strptime(arg_time, "%H:%M", &buf) != NULL)) {
        sprintf(str_time, "%02d:%02d", buf.tm_hour, buf.tm_min);
    }

    if (arg_job != NULL) {
        sprintf(str_time, "%02d:%02d", buf.tm_hour, buf.tm_min);
    }

    fp_history = fopen(tkeep_path_history, "r");
    if (fp_history == NULL) {
        printf("err: read history file [.tkeep_history]\n");
    }

    while (fgets(line, CHAR_LEN, fp_history) != NULL) {
        n_line++;
        if (atoi(arg_line) == n_line) {
            if (arg_day != NULL) {
                memcpy(line, str_day, DAY_LEN);
            }
            if (arg_time != NULL) {
                memcpy(line + DAY_LEN + 1 + TIME_LEN + 1, str_time, TIME_LEN);
            }
            if (arg_job != NULL) {
                sprintf(line + DAY_LEN + 1 + (TIME_LEN + 1) * 2, "%d\n", atoi(arg_job));
            }
            sprintf(view, "%s%s", view, line);
        } else {
            sprintf(view, "%s%s", view, line);
        }
        memset(line, 0, CHAR_LEN);
    }
    fclose(fp_history);

    fp_history = fopen(tkeep_path_history, "w");
    if (fp_history == NULL) {
        printf("err: read history file [.tkeep_history]\n");
    }

    fputs(view, fp_history);
    fclose(fp_history);

    free(view);
}

static void add_history(char* arg_job, char* arg_time)
{
    FILE* fp_history = NULL;
    time_t t = time(NULL);
    struct tm* now = localtime(&t);
    struct tm buf;
    char* view = NULL;
    char line[CHAR_LEN];
    char str_time[TIME_LEN + 1];
    int n_line = 0;

    view = (char*)malloc(VIEW_LEN);
    memset(view, 0, VIEW_LEN);
    memset(line, 0, CHAR_LEN);
    memset(str_time, 0, TIME_LEN + 1);

    if ((arg_time != NULL) && (strptime(arg_time, "%H:%M", &buf) != NULL)) {
        sprintf(str_time, "%02d:%02d", buf.tm_hour, buf.tm_min);
    } else {
        sprintf(str_time, "99:99");
    }

    fp_history = fopen(tkeep_path_history, "r");
    if (fp_history == NULL) {
        printf("err: read history file [.tkeep_history]\n");
    }

    while (fgets(line, CHAR_LEN, fp_history) != NULL) {
        sprintf(view, "%s%s", view, line);
        memset(line, 0, CHAR_LEN);
    }
    fclose(fp_history);

    fp_history = fopen(tkeep_path_history, "w");
    fprintf(fp_history, "%04d/%02d/%02d %02d:%02d %s %d\n%s",
        now->tm_year + 1900,
        now->tm_mon + 1,
        now->tm_mday,
        now->tm_hour,
        now->tm_min,
        str_time,
        atoi(arg_job),
        view);
    fclose(fp_history);

    free(view);
}

static void read_history(char* day)
{
    FILE* fp_history = NULL;
    char* view = NULL;
    char* temp = NULL;
    char line[CHAR_LEN];
    char str_day[DAY_LEN];
    int n_line = 0;

    view = (char*)malloc(VIEW_LEN);
    temp = (char*)malloc(VIEW_LEN);
    memset(view, 0, VIEW_LEN);
    memset(temp, 0, VIEW_LEN);
    memset(line, 0, CHAR_LEN);
    memset(str_day, 0, DAY_LEN);

    fp_history = fopen(tkeep_path_history, "r");
    if (fp_history == NULL) {
        printf("err: read history file [.tkeep_history]\n");
    }

    // set default str_day
    if (day == NULL) {
        if (fgets(line, CHAR_LEN, fp_history) != NULL) {
            strncpy(str_day, line, DAY_LEN);
            n_line++;
            sprintf(view, "%05d: %s", n_line, read_line(line));
            memset(line, 0, CHAR_LEN);
        }
    } else {
        strncpy(str_day, day, DAY_LEN);
    }

    // read lines
    while (fgets(line, CHAR_LEN, fp_history) != NULL) {
        n_line++;
        if (!memcmp(line, str_day, DAY_LEN)) {
            memcpy(temp, view, VIEW_LEN);
            sprintf(view, "%05d: %s%s", n_line, read_line(line), temp);
            memset(temp, 0, VIEW_LEN);
            memset(line, 0, CHAR_LEN);
        }
    }
    fclose(fp_history);

    printf("%s", view);
    free(view);
    free(temp);
}

static char* read_line(char* line)
{
    char temp[CHAR_LEN];
    char job[CHAR_LEN];

    memset(temp, 0, CHAR_LEN);
    memset(job, 0, CHAR_LEN);

    memcpy(temp, line, CHAR_LEN);
    memset(line, 0, CHAR_LEN);

    parse_job(temp + 23, job);
    if (!memcmp(temp + 17, "99:99", 5)) {
        memcpy(line, temp, 16);
    } else {
        memcpy(line, temp, 11);
        memcpy(line + 11, temp + 17, 5);
    }
    sprintf(line + 16, " %s", job);
    return line;
}

static void parse_job(char* id, char* str)
{
    FILE* fp_config;
    char line[CHAR_LEN];
    int n_line = 0;

    memset(line, 0, CHAR_LEN);

    fp_config = fopen(tkeep_path_config, "r");
    if (fp_config == NULL) {
        printf("err: parse config file [.tkeep]\n");
    }

    sprintf(str, "%s", "---\n");
    while (fgets(line, CHAR_LEN, fp_config) != NULL) {
        n_line++;
        if (n_line == atoi(id)) {
            sprintf(str, "[%d] %s", n_line, line);
        }
        memset(line, 0, CHAR_LEN);
    }
}

static int delete_history(int d_line, char* str_day)
{
    FILE* fp_history = NULL;
    char* view = NULL;
    char line[CHAR_LEN];
    int n_line = 0;
    int res = 0;

    if (d_line == 0) {
        return res;
    }

    view = (char*)malloc(VIEW_LEN);
    memset(view, 0, VIEW_LEN);
    memset(line, 0, CHAR_LEN);
    memset(str_day, 0, DAY_LEN);

    fp_history = fopen(tkeep_path_history, "r");
    if (fp_history == NULL) {
        printf("err: read history file [.tkeep_history]\n");
    }

    // read lines
    while (fgets(line, CHAR_LEN, fp_history) != NULL) {
        n_line++;
        if (n_line == d_line) {
            strncpy(str_day, line, DAY_LEN);
            res = 1;
        } else {
            sprintf(view, "%s%s", view, line);
        }
    }
    fclose(fp_history);

    fp_history = fopen(tkeep_path_history, "w");
    if (fp_history == NULL) {
        printf("err: read history file [.tkeep_history]\n");
    }

    fputs(view, fp_history);
    fclose(fp_history);

    free(view);
    return res;
}

static void init_paths()
{
    char* path_home;
    FILE* fp_config;

    memset(tkeep_path_config, 0, CHAR_LEN);
    memset(tkeep_path_history, 0, CHAR_LEN);

    path_home = getenv("HOME");
    snprintf(tkeep_path_config, CHAR_LEN, "%s/.tkeep", path_home);
    snprintf(tkeep_path_history, CHAR_LEN, "%s/.tkeep_history", path_home);

    // init config file
    fp_config = fopen(tkeep_path_config, "r");
    if (fp_config == NULL) {
        // make default config file
        fp_config = fopen(tkeep_path_config, "a");
        if (fp_config == NULL) {
            printf("err: make config file [.tkeep]\n");
        }
        fprintf(fp_config, "invalid\n");
        printf("make config file [.tkeep]\n");
        fclose(fp_config);
    }
    fp_config = fopen(tkeep_path_history, "r");
    if (fp_config == NULL) {
        // make default config file
        fp_config = fopen(tkeep_path_history, "a");
        if (fp_config == NULL) {
            printf("err: make history file [.tkeep_history]\n");
        }
        printf("make history file [.tkeep_history]\n");
        fclose(fp_config);
    }
}
